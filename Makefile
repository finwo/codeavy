CC ?= gcc
CFLAGS ?= -O2 -s -march=native
LIBS ?=

.PHONY: all
all: gwsocket

.c.o:
	$(CC) -c -o $@ $< $(CFLAGS)

gwsocket: patches
	cd src/gwsocket; \
		autoreconf -fiv; \
		./configure; \
		make; \
		cp gwsocket ../../gwsocket

.PHONY: patches
patches: submodules
	@- $(foreach PATCH,$(wildcard patch/**/*.patch), \
		patch -d $(patsubst patch/%.patch,src/%.patch,$(PATCH)) -i ../../$(PATCH); \
  )

.PHONY: submodules
submodules:
	git submodule update --init --recursive
	git submodule foreach git reset --hard
	git submodule foreach git clean -fd

.PHONY: clean
clean:
	$(foreach OBJECT,$(wildcard src/**/*.o), \
		rm $(OBJECT); \
	)
